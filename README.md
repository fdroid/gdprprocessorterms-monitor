
# Monitor the Google "GDPR Processor Terms" license

This checks the
[license on a Google URL](https://privacy.google.com/businesses/gdprprocessorterms)
and displays any differences since we last agreed to it.

https://gitlab.com/fdroid/fdroidserver/issues/620
